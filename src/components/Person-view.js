import React, {Component} from 'react';

class PersonView extends Component {

    constructor(props){
        super(props);
        this.state = {
            isEditing : false,
        }
    }

    handleClick = ()=>{
        this.props.delete(this.props.person);
    };

    editMode = () =>{
        this.setState((state)=>{
            return {isEditing: true}
        })
    };

    handleSaveEditing = () =>{
        if(this.name.value!=='' && this.surname.value!=='') {
            this.props.edit(this.props.person, this.name.value, this.surname.value);
            this.setState((state) => {
                return {isEditing: false}
            })
        }

    };
    handleBack = () =>{
        this.setState((state)=>{
            return {isEditing: false}
        });
    };


    render() {
        if (this.state.isEditing === false) {
            return (
                <div className={"container"}>
                    <div className={"row"}>
                        <div className={"col-3"}>{this.props.person.id}</div>
                        <div className={"col-3"}>{this.props.person.firstname}</div>
                        <div className={"col-3"}>{this.props.person.lastname}</div>
                        <div className={"col-1"}>
                            <button onClick={this.handleClick} type={"submit"} className={"btn btn-danger"}>delete
                            </button>
                        </div>
                        <div className={"col-2"}>
                            <button onClick={this.editMode} type={"submit"} className={"btn btn-warning"}><i className="fas fa-user-edit"></i></button>
                        </div>
                    </div>
                </div>
            );
        }
        else{
            return (
                <div className={"container"}>
                    <div className={"row"}>
                        <div className={"col-3"}>{this.props.person.id}</div>
                        <input className={"col-3"} defaultValue={this.props.person.firstname} ref={(input) => this.name = input} required/>
                        <input className={"col-3"} defaultValue={this.props.person.lastname} ref={(input) => this.surname = input}/>
                        <div className={"col-3"}>
                            <button onClick={this.handleSaveEditing} type={"submit"} className={"btn btn-warning"}>Save</button>
                            <button onClick={this.handleBack} type={"submit"} className={"btn btn-danger"}>Back</button>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default PersonView;
