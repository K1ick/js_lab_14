import React, {Component} from 'react';
import PersonView from "./components/Person-view";
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import PersonAdd from "./components/Person-add";


class App extends Component {
  constructor(props){
    super(props);
    this.state={
        namefilter:'',
        surnamefilter:'',
      persons : [  ],
        personstorender: []
    }
  }


  componentDidMount() {
      let url = "http://localhost:3001/persons";
      fetch(url)
          .then(resp=>resp.json())
          .then(data=>{
              this.setState({persons:data});
              this.Filter();
          });
  }

    addPerson = (name, surname)=>{
      let id = this.state.persons[this.state.persons.length-1].id+1;
      this.setState((state) =>{
        state.persons.push({
          'firstname':name,
          'lastname':surname,
          'id':id
        });
          fetch('http://localhost:3001/persons', {
              method: 'POST',
              mode: 'cors',
              cache: 'no-cache',
              credentials: 'same-origin',
              headers: {
                  'Content-Type': 'application/json',

              },
              redirect: 'follow',
              referrer: 'no-referrer',
              body: JSON.stringify({
                  'firstname':name,
                  'lastname':surname,
                  'id':id
              }),
          })
              .then(response => response.json());
        return {persons: state.persons}
      });
    };

  delPerson = (person)=>{
      let delindex =-1;
      this.state.persons.forEach((elem, index)=>{
          if(elem.id===person.id){
              delindex=index;
          }
      });
        this.setState((state) =>{
            state.persons.splice(delindex, 1);
            fetch('http://localhost:3001/persons/'+person.id, {
                method: 'DELETE',
                mode: 'cors',
                cache: 'no-cache',
                credentials: 'same-origin',
                headers: {
                    'Content-Type': 'application/json',

                },
                redirect: 'follow',
                referrer: 'no-referrer',

            })
                .then(response => response.json());
            return {persons: state.persons};
        });
      this.Filter();
  };

  editPerson = (person, firstname, surname) =>{
      let delindex =-1;
      this.state.persons.forEach((elem, index)=>{
          if(elem.id===person.id){
             delindex=index;
          }
      });
      this.setState((state)=>{
          state.persons[delindex]={
              'firstname':firstname,
              'lastname':surname,
              'id':this.state.persons[delindex].id
          };
          fetch('http://localhost:3001/persons/'+this.state.persons[delindex].id, {
              method: 'PATCH',
              mode: 'cors',
              cache: 'no-cache',
              credentials: 'same-origin',
              headers: {
                  'Content-Type': 'application/json',

              },
              redirect: 'follow',
              referrer: 'no-referrer',
              body: JSON.stringify({
                  'firstname':firstname,
                  'lastname':surname,
                  'id':this.state.persons[delindex].id
              }),
          })
              .then(response => response.json());
          return {persons: state.persons}
      });
      this.Filter();
  };


  Filter = () =>{
      console.log("start");
      this.setState((state)=>{
         return {namefilter: this.nameFilter.value,
                surnamefilter: this.surnameFilter.value}
      });
      console.log(this.nameFilter.value+"  "+this.surnameFilter.value);
      if(this.surnameFilter.value==='' && this.nameFilter.value!=='') {
          console.log(this.nameFilter.value);
          this.setState((state) => {
              state.personstorender = [];
              state.persons.forEach(elem => {
                  if (elem.firstname.startsWith(this.nameFilter.value)) {
                      console.log(elem);
                      state.personstorender.push(elem)
                  }
                  return {personstorender: state.personstorender}
              })
          })
      }else if(this.surnameFilter.value!=='' && this.nameFilter.value!==''){
          this.setState((state) => {
              state.personstorender = [];
              state.persons.forEach((elem) => {
                  console.log(state.personstorender);
                  if (elem.firstname.startsWith(this.nameFilter.value) && elem.lastname.startsWith(this.surnameFilter.value)) {
                      console.log(elem);
                      state.personstorender.push(elem)
                  }
                  return {personstorender: state.personstorender}
              })
          })

      }else if(this.surnameFilter.value!=='' && this.nameFilter.value===''){
          this.setState((state) => {
              state.personstorender = [];

              state.persons.forEach(elem => {
                  if (elem.lastname.startsWith(this.surnameFilter.value)) {
                      state.personstorender.push(elem)
                  }
                  return {personstorender: state.personstorender}
              })
          })
      }
      else{
          this.setState((state) => {
              state.personstorender = state.persons;
                  return {personstorender: state.personstorender}
              })
      }
  };


  render() {
    const listItems = this.state.personstorender.map((person) =>
        <PersonView person={person} edit={this.editPerson} delete={this.delPerson}></PersonView>
    );
    return (
        <div className="App">
            <label>Name <input type={"text"} placeholder={"Ivan"} onChange={this.Filter} ref={(input)=> this.nameFilter = input} /></label>
            <label>Surname <input type={"text"} placeholder={"Ivanov"} onChange={this.Filter} ref={(input)=> this.surnameFilter = input} /></label>
          {listItems}
          <PersonAdd addPerson={this.addPerson}></PersonAdd>
        </div>
    );
  }

}

export default App;
